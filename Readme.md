#Transaction Service

# Application Initialization

-  Spring boot starts using `gradle bootRun` 
-  Unit tests runs using `gradle clean test`

# Endpoints available :
- Transaction can be created using `/transactions` POST Endpoint.
  Request Body:
  
        {
            "amount": 100.5,
            "timestamp": ​1478192204000
        }
         
- Statistics can be fetched using `/statistics` GET Endpoint.
  Response Body: 

        {        ​
            "sum":​ ​1000, 
            ​"avg":​ ​100, 
            ​"max":​ ​200, ​
            "min":​ ​50, 
            ​"count":​ ​10
        }
        
        