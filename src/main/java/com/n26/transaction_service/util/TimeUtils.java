package com.n26.transaction_service.util;

import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class TimeUtils {

    public static boolean isOlder(long timestamp) {
        return Instant.now().getEpochSecond() - timestamp > 60;
    }
}
