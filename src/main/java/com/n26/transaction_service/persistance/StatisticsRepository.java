package com.n26.transaction_service.persistance;

import com.n26.transaction_service.model.Statistics;
import com.n26.transaction_service.model.Transaction;
import org.springframework.stereotype.Component;

import java.util.DoubleSummaryStatistics;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Component
public class StatisticsRepository {

    private ConcurrentNavigableMap<Long, DoubleSummaryStatistics> transactionCache;
    private Statistics statistic = new Statistics(0D, 0D, 0D, 0D, 0L);

    public StatisticsRepository() {
        this.transactionCache = new ConcurrentSkipListMap();
    }

    public Statistics getStatistic() {
        return statistic;
    }

    public void add(Transaction transaction) {
        DoubleSummaryStatistics statistics = new DoubleSummaryStatistics();
        statistics.accept(transaction.getAmount());
        transactionCache.merge(transaction.getTimestamp(),
                statistics, (doubleSummaryStatistics, other) -> {
                    doubleSummaryStatistics.combine(other);
                    return doubleSummaryStatistics;
                });
        updateStatistics();
    }

    public void updateStatistics() {
        if (transactionCache.size() > 0) {
            DoubleSummaryStatistics doubleSummaryStatistics = new DoubleSummaryStatistics();
            transactionCache
                    .values()
                    .forEach(doubleSummaryStatistics::combine);

            statistic = new Statistics(doubleSummaryStatistics);
        } else {
            statistic = new Statistics(0D, 0D, 0D, 0D, 0L);
        }
    }

    public ConcurrentNavigableMap<Long, DoubleSummaryStatistics> getTransactionCache() {
        return transactionCache;
    }
}
