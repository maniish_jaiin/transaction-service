package com.n26.transaction_service.scheduler;

import com.n26.transaction_service.persistance.StatisticsRepository;
import com.n26.transaction_service.util.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@EnableScheduling
@Component
public class TransactionScheduler {

    @Autowired
    StatisticsRepository statisticsRepository;

    public TransactionScheduler() {
    }

    @Async
    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    public void cleanOldTransactions() {
        List<Long> invalidTransactions = statisticsRepository
                .getTransactionCache().keySet()
                .stream()
                .filter(TimeUtils::isOlder)
                .collect(Collectors.toList());

        invalidTransactions.forEach(timestamp -> statisticsRepository
                .getTransactionCache().remove(timestamp));

        statisticsRepository.updateStatistics();
    }
}
