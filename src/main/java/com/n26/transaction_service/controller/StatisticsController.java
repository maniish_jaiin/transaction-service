package com.n26.transaction_service.controller;

import com.n26.transaction_service.model.Statistics;
import com.n26.transaction_service.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {
    private Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    StatisticsService statisticsService;

    @GetMapping(value = "/statistics")
    public ResponseEntity getStatisticsForTransactions() {
        log.info("Received statistics request");
        Statistics transactionStatistics = statisticsService.getTransactionStatistics();
        return new ResponseEntity<>(transactionStatistics, HttpStatus.OK);
    }
}
