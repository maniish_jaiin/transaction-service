package com.n26.transaction_service.controller;

import com.n26.transaction_service.model.Transaction;
import com.n26.transaction_service.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionController {

    private Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    TransactionService transactionService;

    @PostMapping(value = "/transactions")
    public ResponseEntity saveTransaction(@RequestBody Transaction transaction) {
        if (!transaction.isValid()) {
            log.info("Received invalid transaction with timestamp:{}", transaction.getTimestamp());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else {
            log.info("Received valid transaction with timestamp:{}", transaction.getTimestamp());
            transactionService.saveTransaction(transaction);
            return new ResponseEntity(HttpStatus.CREATED);
        }
    }
}
