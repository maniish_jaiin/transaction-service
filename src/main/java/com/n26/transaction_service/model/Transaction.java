package com.n26.transaction_service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class Transaction {

    @JsonProperty("amount")
    private double amount;
    @JsonProperty("timestamp")
    private long timestamp;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isValid() {
        return Instant.now().getEpochSecond() - timestamp <= 60;
    }
}
