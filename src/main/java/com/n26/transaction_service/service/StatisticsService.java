package com.n26.transaction_service.service;

import com.n26.transaction_service.model.Statistics;
import com.n26.transaction_service.model.Transaction;
import com.n26.transaction_service.persistance.StatisticsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {

    private Logger log = LoggerFactory.getLogger(StatisticsService.class);

    @Autowired
    StatisticsRepository statisticsRepository;

    public Statistics getTransactionStatistics() {
        return statisticsRepository.getStatistic();
    }


    public void save(Transaction transaction) {
        log.info("Saving transaction with timestamp: {}", transaction.getTimestamp());
        statisticsRepository.add(transaction);
    }
}
