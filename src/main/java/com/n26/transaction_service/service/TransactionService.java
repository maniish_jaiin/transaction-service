package com.n26.transaction_service.service;

import com.n26.transaction_service.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionService {

    private Logger log = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    StatisticsService statisticsService;

    public TransactionService() {
    }

    public void saveTransaction(Transaction transaction) {
        log.info("Received request with transaction with timestamp:{} ", transaction.getTimestamp());
        statisticsService.save(transaction);
    }
}
