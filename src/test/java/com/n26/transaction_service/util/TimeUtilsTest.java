package com.n26.transaction_service.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class TimeUtilsTest {

    @Test
    public void shouldReturnFalseIfTimestampIsOld() {
        boolean older = TimeUtils.isOlder(1528655896);
        assertTrue(older);
    }
}