package com.n26.transaction_service.persistance;

import com.n26.transaction_service.model.Statistics;
import com.n26.transaction_service.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsRepositoryTest {

    @InjectMocks
    StatisticsRepository statisticsRepository;

    @Test
    public void shouldSaveTheTransactionGivenRecentTransaction() {
        Transaction transaction = new Transaction();
        transaction.setAmount(120);
        transaction.setTimestamp(Instant.now().getEpochSecond());
        statisticsRepository.add(transaction);

        Statistics statistic = statisticsRepository.getStatistic();
        assertEquals(120, statistic.getAvg(), 0);
        assertEquals(120, statistic.getMin(), 0);
        assertEquals(120, statistic.getMax(), 0);
        assertEquals(1, statistic.getCount(), 0);
        assertEquals(120, statistic.getSum(), 0);
    }

    //
    @Test
    public void shouldAddAllRecentMultipleTransactions() {
        Transaction transaction = new Transaction();
        Transaction transaction2 = new Transaction();
        transaction.setAmount(120);
        transaction.setTimestamp(Instant.now().getEpochSecond());
        transaction2.setAmount(200);
        transaction2.setTimestamp(Instant.now().getEpochSecond());
        statisticsRepository.add(transaction);
        statisticsRepository.add(transaction2);

        Statistics statistic = statisticsRepository.getStatistic();
        assertEquals(160, statistic.getAvg(), 0);
        assertEquals(120, statistic.getMin(), 0);
        assertEquals(200, statistic.getMax(), 0);
        assertEquals(2, statistic.getCount(), 0);
        assertEquals(320, statistic.getSum(), 0);
    }
}