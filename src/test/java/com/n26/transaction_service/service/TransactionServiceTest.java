package com.n26.transaction_service.service;

import com.n26.transaction_service.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private StatisticsService statisticsService;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    public void shouldCallSaveOfStatisticsService() {
        Transaction recentTransaction = new Transaction();
        recentTransaction.setAmount(135);
        recentTransaction.setTimestamp(Instant.now().minusSeconds(30).getEpochSecond());

        transactionService.saveTransaction(recentTransaction);

        verify(statisticsService).save(recentTransaction);
    }
}