package com.n26.transaction_service.service;

import com.n26.transaction_service.model.Transaction;
import com.n26.transaction_service.persistance.StatisticsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {

    @Mock
    StatisticsRepository statisticsRepository;

    @InjectMocks
    private StatisticsService statisticsService;

    @Mock
    Transaction transaction;

    @Test
    public void shouldAddOrUpdateToCacheWhenRequestIsReceived() {
        statisticsService.save(transaction);

        verify(statisticsRepository).add(any());
    }
}