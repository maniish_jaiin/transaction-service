package com.n26.transaction_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transaction_service.model.Statistics;
import com.n26.transaction_service.service.StatisticsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class StatisticControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private StatisticsController statisticsController;

    @Mock
    private StatisticsService statisticsService;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(statisticsController).build();
    }

    @Test
    public void shouldRespondWithStatisticsForLast60Seconds() throws Exception {

        Statistics statistics = new Statistics(100, 10, 10, 10, 10);
        when(statisticsService.getTransactionStatistics()).thenReturn(statistics);

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(statistics)));
    }

}