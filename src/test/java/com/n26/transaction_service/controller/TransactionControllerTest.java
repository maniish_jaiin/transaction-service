package com.n26.transaction_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transaction_service.model.Transaction;
import com.n26.transaction_service.service.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static java.time.Instant.now;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private TransactionController transactionController;

    @Mock
    private TransactionService transactionService;

    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(transactionController).build();
    }

    @Test
    public void shouldRespondWithTransactionCreatedResponce() throws Exception {
        Transaction recentTransaction = new Transaction();
        recentTransaction.setAmount(125);
        recentTransaction.setTimestamp(now().getEpochSecond());

        mockMvc.perform(
                post("/transactions")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(recentTransaction))
        ).andExpect(status().isCreated());
    }

    @Test
    public void shouldRespondWithEmptyContentWhenTransactionIsDiscarded() throws Exception {
        Transaction oldTransaction = new Transaction();
        oldTransaction.setAmount(125);
        oldTransaction.setTimestamp(now().minusSeconds(61).getEpochSecond());

        mockMvc.perform(
                post("/transactions")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(oldTransaction))
        ).andExpect(status().isNoContent());
    }
}