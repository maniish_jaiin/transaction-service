package com.n26.transaction_service.model;

import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TransactionTest {

    @Test
    public void shouldReturnTrueIfTransactionIsWithin60Seconds() {
        Transaction transaction = new Transaction();
        transaction.setAmount(120);
        transaction.setTimestamp(Instant.now().getEpochSecond());

        assertTrue(transaction.isValid());
    }

    @Test
    public void shouldReturnFalseIfTransactionIsOlderThan60Seconds() {
        Transaction transaction = new Transaction();
        transaction.setAmount(120);
        transaction.setTimestamp(Instant.now().minusSeconds(61).getEpochSecond());
        assertFalse(transaction.isValid());
    }

}